package com.reezbhan.recyclerview.Activity.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reezbhan.recyclerview.Activity.Model.ListItem;
import com.reezbhan.recyclerview.R;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

   private List<ListItem> listItems;
   private Context context;

    public MyAdapter(List<ListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ListItem listItem = listItems.get(position);

        holder.textviewhead.setText(listItem.getHead());
        holder.textviewdesc.setText(listItem.getDesc());

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textviewhead;
        public TextView textviewdesc;

        public ViewHolder(View itemView) {
            super(itemView);

            textviewhead = (TextView) itemView.findViewById(R.id.textviewhead);
            textviewdesc = (TextView) itemView.findViewById(R.id.textviewdesc);
        }
    }
}
