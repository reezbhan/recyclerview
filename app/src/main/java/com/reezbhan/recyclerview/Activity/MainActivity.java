package com.reezbhan.recyclerview.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.reezbhan.recyclerview.Activity.Adapter.MyAdapter;
import com.reezbhan.recyclerview.Activity.Model.ListItem;
import com.reezbhan.recyclerview.Activity.Model.Stadium;
import com.reezbhan.recyclerview.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<ListItem> listItems;
    private SQLiteHandler sqLiteHandler;
    private List<Stadium> stadiumList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true); //set list items true
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        sqLiteHandler = new SQLiteHandler(MainActivity.this);
        stadiumList = sqLiteHandler.getStadiumDetails();
        Log.e(TAG, stadiumList.get(0).getName());

        //getRequest();

        listItems = new ArrayList<>();

        for (int i = 0; i <= 10; i++) {
            ListItem listItem = new ListItem(
                    "heading " + (i + 1),
                    "Lorem ipsum dummy text"
            );

            listItems.add(listItem);
        }

        adapter = new MyAdapter(listItems, this);

        recyclerView.setAdapter(adapter);
    }

    private void getRequest() {

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);

        String requestURL = "https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json";

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, response.toString());
                try {
                    JSONArray stadiums = response.getJSONArray("stadiums");

                    for (int i = 0; i < stadiums.length(); i++) {
                        JSONObject stadium = stadiums.getJSONObject(i);

                        String name = stadium.getString("name");
                        String city = stadium.getString("city");
                        int id = stadium.getInt("id");
                        Log.e(TAG, name);
                        Log.e(TAG, city);

                        sqLiteHandler.addStadium(name, city, String.valueOf(id));
                    }

                    //team......

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Log.e(TAG, "" + error);
            }
        });

        requestQueue.add(jsonObjectRequest);
    }
}
